# Ohos_Link_Preview

**本项目是基于开源项目Android-Link-Preview进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/LeonardoCardoso/Android-Link-Preview）追踪到原项目版本**

#### 项目介绍

- 项目名称：链接预览
- 所属系列：ohos的第三方组件适配移植
- 功能：Ohos_Link_Preview可以从一个url进行预览，获取所有信息，如标题、相关文本和图像
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：
- 联系方式：
- 原项目Doc地址：
- 原项目基线版本：v1.1
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="ohos-link-preview.gif"/>

#### 安装教程

方法1.

1. 编译har包Ohos_Link_Preview.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'io.github.dzsf:link-preview:1.0.1'
}
```

#### 使用说明

1. 创建一个RetroJsoup
通过url的元数据发出请求，将在后台线程中执行，从LinkPreview获取一个实例或得到一个exception，onSuccess、onFailed在后台线程中执行，使用处理程序访问UI对象

```
LinkUtil.getInstance().getLinkPreview(Context, String url, GetLinkPreviewListener)
```

2. LinkPreview实现类

```
public class LinkPreview implements Serializable {

    String title, description, link, siteName, imageUrl;
    File imageFile;
    ...
}
```

3. GetLinkPreviewListener接口

```
public interface GetLinkPreviewListener {
    void onSuccess(LinkPreview link);
    void onFailed(Exception e);
}
```

#### 版本迭代

- v1.0.1

#### 版权和许可信息

