package com.mega4tech.ohoslinkpreview;

import com.mega4tech.linkpreview.GetLinkPreviewListener;
import com.mega4tech.linkpreview.LinkPreview;
import com.mega4tech.linkpreview.LinkUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.Test;

public class LinkUtilTest {

    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x002, "MY_TAG");

    @Test
    public void getLinkPreview() throws InterruptedException {

        final LinkPreview[] linkPreview = {new LinkPreview()};

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinkUtil.getInstance().getLinkPreview(context, "https://www.tmall.com/", new GetLinkPreviewListener() {
            @Override
            public void onSuccess(LinkPreview link) {
                linkPreview[0] = link;
            }

            @Override
            public void onFailed(Exception e) {
            }
        });

        Thread.sleep(20000);

        HiLog.error(label, "Title+++++++++++++++++++++" + linkPreview[0].getTitle());
        HiLog.error(label, "Description+++++++++++++++++++++" + linkPreview[0].getDescription());
        HiLog.error(label, "Link+++++++++++++++++++++" + linkPreview[0].getLink());
        HiLog.error(label, "ImageUrl+++++++++++++++++++++" + linkPreview[0].getImageUrl());

        Assert.assertEquals(linkPreview[0].getTitle(), "天猫tmall.com--理想生活上天猫");
        Assert.assertEquals(linkPreview[0].getDescription(), "天猫，中国线上购物的地标网站，亚洲超大的综合性购物平台，拥有10万多品牌商家。每日发布大量国内外商品！正品网购，上天猫！天猫千万大牌正品,品类全，一站购，支付安全，退换无忧！理想生活上天猫!");
        Assert.assertEquals(linkPreview[0].getLink(), " https://www.tmall.com/");
        Assert.assertEquals(linkPreview[0].getImageUrl(), " https://img.alicdn.com/tfs/TB1MaLKRXXXXXaWXFXXXXXXXXXX-480-260.png");

    }

    @Test
    public void getLinkPreview2() throws InterruptedException {

        final LinkPreview[] linkPreview = {new LinkPreview()};

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinkUtil.getInstance().getLinkPreview(context, "https://www.clove.co.uk/", new GetLinkPreviewListener() {
            @Override
            public void onSuccess(LinkPreview link) {
                linkPreview[0] = link;
            }

            @Override
            public void onFailed(Exception e) {
            }
        });

        Thread.sleep(20000);

        HiLog.error(label, "Title+++++++++++++++++++++" + linkPreview[0].getTitle());
        HiLog.error(label, "Description+++++++++++++++++++++" + linkPreview[0].getDescription());
        HiLog.error(label, "Link+++++++++++++++++++++" + linkPreview[0].getLink());
        HiLog.error(label, "ImageUrl+++++++++++++++++++++" + linkPreview[0].getImageUrl());

        Assert.assertEquals(linkPreview[0].getTitle(), "Clove Technology - Shipping Smartphones, Tablets & Accessories");
        Assert.assertEquals(linkPreview[0].getDescription(), "Clove offers low prices on the latest SIM Free Smartphones, Tablets, Games Consoles and accessories. Order online securely for worldwide shipping.");
        Assert.assertEquals(linkPreview[0].getLink(), "https://www.clove.co.uk/");
        Assert.assertEquals(linkPreview[0].getImageUrl(), null);

    }

    @Test
    public void getLinkPreviewRespFailure() throws InterruptedException {

        final Exception[] exception = {new Exception()};

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinkUtil.getInstance().getLinkPreview(context, "https://www.amazon.com/gp/aw/d", new GetLinkPreviewListener() {
            @Override
            public void onSuccess(LinkPreview link) {
            }

            @Override
            public void onFailed(Exception e) {
                exception[0] = e;
            }
        });

        Thread.sleep(20000);

        HiLog.error(label, "Exception+++++++++++++++++++++" + exception[0].getMessage());
        Assert.assertEquals(exception[0].getMessage(), "Unexpected code Response{protocol=h2, code=404, message=, url=https://www.amazon.com/gp/aw/d}");

    }

    @Test
    public void getLinkPreviewEmptyUrl() throws InterruptedException {

        final Exception[] exception = {new Exception()};

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinkUtil.getInstance().getLinkPreview(context, "", new GetLinkPreviewListener() {
            @Override
            public void onSuccess(LinkPreview link) {
            }

            @Override
            public void onFailed(Exception e) {
                exception[0] = e;
            }
        });

        Thread.sleep(5000);

        HiLog.error(label, "Exception+++++++++++++++++++++" + exception[0].getMessage());
        Assert.assertEquals(exception[0].getMessage(), "URL should not be null or empty");

    }

    @Test
    public void getLinkPreviewInvalidUrl() throws InterruptedException {

        final Exception[] exception = {new Exception()};

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinkUtil.getInstance().getLinkPreview(context, "aaaaaaaaaaaaa", new GetLinkPreviewListener() {
            @Override
            public void onSuccess(LinkPreview link) {
            }

            @Override
            public void onFailed(Exception e) {
                exception[0] = e;
            }
        });

        Thread.sleep(5000);

        HiLog.error(label, "Exception+++++++++++++++++++++" + exception[0].getMessage());
        Assert.assertEquals(exception[0].getMessage(), "unexpected url: aaaaaaaaaaaaa");

    }

}