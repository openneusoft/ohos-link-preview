package com.mega4tech.ohoslinkpreview.slice;

import com.isotne.glidelibrary.utils.OhosGlide;
import com.mega4tech.linkpreview.GetLinkPreviewListener;
import com.mega4tech.linkpreview.LinkPreview;
import com.mega4tech.linkpreview.LinkUtil;
import com.mega4tech.ohoslinkpreview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {

    private Context context;
    private AbilitySlice abilitySlice;
    private EventHandler eventHandler;
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x002, "MY_TAG");

    private TextField linkEt;
    private Button linkFetchBtn;
    private DirectionalLayout previewGroup;
    private Image imgPreviewIv;
    private Text titleTv;
    private Text descTv;
    private Text siteTv;
    private ProgressBar progress;
    private ToastDialog toastDialog;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();

        linkFetchBtn.setClickedListener(component -> {

            LinkUtil.getInstance().getLinkPreview(context, linkEt.getText(), new GetLinkPreviewListener() {
                @Override
                public void onSuccess(final LinkPreview link) {
                    eventHandler.postTask(() -> {
                        printPageSuccess(link);
                    });
                }

                @Override
                public void onFailed(Exception e) {
                    eventHandler.postTask(() -> {
                        printPageFailed(e);
                    });
                }
            });
        });
    }

    private void printPageSuccess(LinkPreview link) {
        HiLog.error(label, "Title*******：" + link.getTitle());
        HiLog.error(label, "Description*******：" + link.getDescription());
        HiLog.error(label, "SiteName*******：" + link.getSiteName());
        HiLog.error(label, "Link*******：" + link.getLink());
        HiLog.error(label, "ImageUrl*******：" + link.getImageUrl());

        //progress.setVisibility(Component.VISIBLE);
        previewGroup.setVisibility(Component.VISIBLE);
        titleTv.setText(link.getTitle() != null ? link.getTitle() : "" );

        descTv.setText(link.getDescription() != null ? link.getDescription() : "" );
        siteTv.setText(link.getSiteName() != null ? link.getSiteName() : "" );
        previewGroup.setClickedListener(component2 -> {
            Intent browserIntent = new Intent();
            browserIntent.setUri(Uri.parse(linkEt.getText()));
            startAbility(browserIntent);
        });
//        link.setImageUrl("https://img.tukuppt.com//ad_preview/00/24/03/5f6584224042a.jpg");
        if(link.getImageUrl() != null) {
            try {
                OhosGlide.with(abilitySlice).load(link.getImageUrl()).def(ResourceTable.Media_icon).into(imgPreviewIv);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void printPageFailed(Exception e) {
        //progress.setVisibility(Component.VISIBLE);
        previewGroup.setVisibility(Component.VISIBLE);
        toastDialog.setText(e.getMessage()).show();
    }

    private void initView() {
        context = getContext();
        abilitySlice = this;
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());

        linkEt = (TextField) findComponentById(ResourceTable.Id_link_et);
        linkFetchBtn = (Button) findComponentById(ResourceTable.Id_link_fetch_btn);
        previewGroup = (DirectionalLayout) findComponentById(ResourceTable.Id_preview_group);
        imgPreviewIv = (Image) findComponentById(ResourceTable.Id_img_preview_iv);
        titleTv = (Text) findComponentById(ResourceTable.Id_title_tv);
        descTv = (Text) findComponentById(ResourceTable.Id_desc_tv);
        siteTv = (Text) findComponentById(ResourceTable.Id_site_tv);
        progress = (ProgressBar) findComponentById(ResourceTable.Id_progress);
        toastDialog = new ToastDialog(context);

        linkEt.setText("https://www.tmall.com/");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
